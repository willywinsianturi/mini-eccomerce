<?php
require_once('koneksi.php');

if (isset($_GET['id'])) {
    $select = mysqli_query($con, "SELECT * FROM tb_produk WHERE id = " . $_GET['id']);
    $row = mysqli_fetch_array($select);
    $nama = $row["nama"];
    $harga = $row["harga"];

    // $id = $_GET['id'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Produk</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="user.php"> User </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_supplier.php"> Supplier </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="col-md-12 offset-md-3">
        <div class="col-6">
            <center>
                <h1>Ubah Produk</h1>
            </center>
            <hr>
        </div>
        <br>

        <div class="container">
            <div class="row">
                <div class="col-md-3 offset-md-5">
                    <a href="lihat_produk.php">Kembali</a>
                </div>
            </div>
        </div>

        <br>

        <form name="form1" method="POST" action="">

            <!-- <div class="form-group row">
                <label for="id" class="col-sm-2 col-form-label"> id Produk </label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="id" value="<?= $id; ?>" required>
                </div>
            </div> -->

            <input type="hidden" value="<?= $_GET['id']; ?>" name="id">

            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label"> Nama Produk </label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="nama" value="<?= $nama; ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="harga" class="col-sm-2 col-form-label"> Harga </label>
                <div class="col-sm-3">
                    <input type="number" class="form-control" name="harga" value="<?= $harga; ?>" min="0" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-1">
                    <button class="btn btn-primary" type="submit" name="submit">Ubah</button>
                </div>
            </div>

            <div class="col-6">
                <?php
                if (isset($_POST['submit'])) {
                    $nama = $_POST["nama"];
                    $harga = $_POST["harga"];

                    $id = $_POST["id"];

                    function tampilAlert($text, $tipe)
                    {
                        echo "<div class=\"alert alert-" . $tipe . "\" role=\"alert\">
                                    <p>" . $text . "</p>
                                </div>";
                    }

                    $update = mysqli_query($con, "UPDATE tb_produk SET nama = '$nama', harga = '$harga' WHERE id = '$id'");
                    if ($update) {
                        tampilAlert("Supplier berhasil ditambahkan..", "success");
                    } else {
                        tampilAlert("Supplier gagal ditambahkan...", "danger");
                    }
                }
                ?>
            </div>

        </form>
    </div>

    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
</body>

<footer class="sticky-footer bg-white" style="position: absolute; bottom: 0; width: 100%; height: 30px;">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
        </div>
    </div>
</footer>

</html>