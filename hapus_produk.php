<?php
require_once('koneksi.php');

if (isset($_GET['id'])) {
    $delete = mysqli_query($con, "DELETE FROM tb_produk WHERE id = " . $_GET['id']);
    if ($delete) {
        echo "<font color=green> Data Berhasil Dihapus </font>";
    } else {
        echo "<font color=red> Data Gagal Dihapus </font>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hapus Produk</title>
</head>

<body>
    <a href="lihat_produk.php">Kembali</a>
</body>

</html>