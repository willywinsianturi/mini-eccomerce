<?php
require_once('koneksi.php');

if (isset($_GET['id'])) {
	$select_beli = mysqli_query($con, "SELECT * FROM tb_beli WHERE id = " . $_GET['id']);
	$row_beli = mysqli_fetch_array($select_beli);
	$tanggal = $row_beli["tanggal"];
	$idsupplier = $row_beli["idsupplier"];
	$iddetailproduk = $row_beli["iddetailproduk"];
	$qty = $row_beli["qty"];
	$harga = $row_beli["harga"];
	$biaya = $row_beli["biaya"];
	$diskon = $row_beli["diskon"];
	$total = $row_beli["total"];
}

if (isset($_POST['submit'])) {
	$tanggal = $_POST["tanggal"];
	$idsupplier = $_POST["idsupplier"];
	$iddetailproduk = $_POST["iddetailproduk"];
	$qty = $_POST["qty"];
	$harga = $_POST["harga"];
	$biaya = $_POST["biaya"];
	$diskon = $_POST["diskon"];
	$total = ($qty * $harga) + $biaya - $diskon;

	$update = mysqli_query($con, "UPDATE tb_beli SET tanggal = '$tanggal', idsupplier = $idsupplier, iddetailproduk = $iddetailproduk, qty = $qty, harga = $harga, biaya = $biaya, diskon = $diskon, total = $total WHERE id = " . $_GET['id']);
	if ($update) {
		$update_stock = mysqli_query($con, "UPDATE tb_detailproduk SET stok = (stok - $row_beli[qty] + $qty) WHERE id = $iddetailproduk");
		// stok = (stok - $row_beli[qty] + $qty) maksudnya adalah stok dikurangi dengan qty yg sebelumnya (qty yg sebelum diubah) baru ditambahkan dengan qty yang baru
		echo "<font color=green> Data Berhasil Diubah </font>";
	} else {
		echo "<font color=red> Data Gagal Diubah </font>";
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ubah Pembelian</title>
</head>

<body>
	<h3>Ubah Pemebelian</h3>
	<a href="lihat_beli.php">Kemballi</a>
	<form action="" method="POST">
		<table width="300" border="0">
			<tr>
				<td>Tanggal</td>
				<td><input type="date" name="tanggal" value="<?= $tanggal; ?>" required></td>
			</tr>
			<tr>
				<td>Supplier</td>
				<td>
					<select name="idsupplier" id="idsupplier">
						<?php
						$select_supplier = mysqli_query($con, "SELECT * FROM tb_supplier");
						while ($row_supplier = mysqli_fetch_array($select_supplier)) {
							if ($row_supplier['id'] == $idsupplier) //$idsupplier diambil dari baris ke 8 di atas
							{
								echo "<option value=$row_supplier[id] selected> $row_supplier[nama] </option>";
							} else {
								echo "<option value=$row_supplier[id]> $row_supplier[nama] </option>";
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<select name="iddetailproduk" id="iddetailproduk">
						<?php
						$select_detailproduk = mysqli_query($con, "SELECT * FROM tb_detailproduk");
						while ($row_detailproduk = mysqli_fetch_array($select_detailproduk)) {
							$select_produk = mysqli_query($con, "SELECT * FROM tb_produk WHERE id = $row_detailproduk[idproduk]");
							$row_produk = mysqli_fetch_array($select_produk);

							$select_warna = mysqli_query($con, "SELECT * FROM tb_warna WHERE id = $row_detailproduk[idwarna]");
							$row_warna = mysqli_fetch_array($select_warna);

							$select_ukuran = mysqli_query($con, "SELECT * FROM tb_ukuran WHERE id = $row_detailproduk[idukuran]");
							$row_ukuran = mysqli_fetch_array($select_ukuran);

							if ($row_detailproduk['id'] == $iddetailproduk) //$iddetailproduk diambil dari baris ke 9 di atas.
							{
								echo "<option value=$row_detailproduk[id]>" . $row_produk['nama'] . " - " . $row_warna['nama'] . " - " . $row_ukuran['nama'] . "</option>";
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Qty</td>
				<td><input type="number" name="qty" min="1" value="<?= $qty; ?>" required /></td>
			</tr>
			<tr>
				<td>Harga</td>
				<td><input type="number" name="harga" min="0" value="<?= $harga; ?>" required /></td>
			</tr>
			<tr>
				<td>Biaya</td>
				<td><input type="number" name="biaya" min="0" value="<?= $biaya; ?>" required /></td>
			</tr>
			<tr>
				<td>Diskon</td>
				<td><input type="number" name="diskon" min="0" value="<?= $diskon; ?>" required /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" value="Submit" /></td>
			</tr>
		</table>
	</form>
</body>

</html>