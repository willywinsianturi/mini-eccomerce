<?php
require_once("koneksi.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lihat Detail Produk</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>

	<!-- NAVBAR -->
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a class="navbar-brand" href="home.php">ApaanSihKak's Shop</a>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a class="nav-link" href="user.php"> User </a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="order.php"> Order </a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="warna.php"> Warna </a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="ukuran.php"> Ukuran </a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="lihat_produk.php"> Produk </a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="about.php"> About Me </a>
			</li>
		</ul>
	</nav>

	<div class="container">
		<div class="col-md-6 offset-md-3">
			<center>
				<h1>Detail Produk</h1>
			</center>
			<hr>
		</div>
		<br>

		<div class="container">
			<div class="row">
				<div class="col-md-3 offset-md-10">
					<a href="tambah_detailproduk.php"><i class="fa fa-plus-circle" aria-hidden="true"></i> Detail Produk</a>
				</div>
			</div>
		</div>

		<table class="table table-hover table-dark">
			<thead>
				<tr align="center">
					<th scope="col">No.</th>
					<th scope="col">Produk</th>
					<th scope="col">Warna</th>
					<th scope="col">Ukuran</th>
					<th scope="col">Stok</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<?php
			$select = mysqli_query($con, "SELECT * FROM tb_detailproduk");
			$i = 1;
			while ($row = mysqli_fetch_array($select)) {
				echo "<tbody>";
				echo "<tr align='center'>";
				echo "<th scope='row'>" . $i++ . "</th>";

				// tampilkan nama produk dari tb_produk, dimana id = idproduk yang didapat dari tb_detailproduk
				$select_produk = mysqli_query($con, "SELECT * FROM tb_produk WHERE id = $row[idproduk]");
				$row_produk = mysqli_fetch_array($select_produk);
				echo "<td> $row_produk[nama] </td>";

				// tampilkan nama warna dari tb_warna, dimana id = idwarna yang didapat dari tb_detailproduk
				$select_warna = mysqli_query($con, "SELECT * FROM tb_warna WHERE id = $row[idwarna]");
				$row_warna = mysqli_fetch_array($select_warna);
				echo "<td> $row_warna[nama] </td>";

				// tampilkan nama ukuran dari tb_ukuran, dimana id = idukuran yang didapat dari tb_detailproduk
				$select_ukuran = mysqli_query($con, "SELECT * FROM tb_ukuran WHERE id = $row[idukuran]");
				$row_ukuran = mysqli_fetch_array($select_ukuran);
				echo "<td> $row_ukuran[nama] </td>";

				// tampilkan stok yang didapat dari tb_detailproduk
				echo "<td> $row[stok] </td>";

				// aksi ubah|hapus
				echo "<td><a href='ubah_detailproduk.php?id=$row[id]'> <i class='fa fa-edit'></i> </a> | <a href='hapus_detailproduk.php?id=$row[id]' onclick='return confirm(\"Anda yakin menghapus data ini?\")'> <i class='fa fa-trash'> </a></td>";
				echo "</tr>";
				echo "</tbody>";
			}
			?>
		</table>
	</div>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>

	<!-- jQuery -->
	<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="AdminLTE/dist/js/adminlte.min.js"></script>

	<script src="fontawesome/js/all.min.js"></script>
	<script src="fontawesome/js/fontawesome.min.js"></script>
</body>

<footer class="sticky-footer bg-white" style="position: absolute; bottom: 0; width: 100%; height: 30px;">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
		</div>
	</div>
</footer>

</html>